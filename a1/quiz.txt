What is the typical naming convention when creating a class?

	Capitalized the starting letter and should be singular.

Should class methods be included in the class constructor?

	No

Can class methods be separated by commas?

	No

Can we update an object’s properties via dot notation?

	Yes

What does a method need to return in order for it to be chainable?

	this